# Nom et structure des données Cayenne

|Capteurs|Type          |Nom variables TAGO|N° de CHANNEL|DATA SIZE|DATA RESOLUTION
|--      |--            |--                |:--:         |:--:     |--|
|DHT22  |Humidity Sensor|DHT22HUM           |1             |1      |0.5 % Unsigned|
|DHT22  |Temperature Sensor|DHT22TEMP       |2              |2  |0.1 °C Signed MSB|
|TSL2561|Illuminance Sensor|TSL2561LUM      |3              |2  |1 Lux Unsigned MSB|
|AirQuality|Analog Input    |AirQUAL        |4              |2  |0.01 Signed valeurs: ok=3, pollué=0|
|BME680     |Temperature Sensor |BME680TEMP |5              |2  |0.1 °C Signed MSB|
|BME680     |Barometer          |BME680PRES |6              |2  |0.1 hPa Unsigned MSB|
|BME680     |Humidity Sensor    |BME680HUM  |7              |1  |0.5 % Unsigned|
|BME680     |Analog Input       |BME680GAZ  |8              |2  |0.01 Signed|

