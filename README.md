# Capteur environnemental connecté au réseau LoRa

## Finalités

Ce code a pour but de piloter un ensemble de capteurs environnementaux afin d'envoyer les données collectées sur un serveur via un réseau LoRa.

Ce projet se place dans le cadre d'un atelier dédié à l'Internet des objets, organisé le 6 juin 2019 de 9h à 11h au TIPI de l'Eurométropole de Strasbourg.

![Logo fablab](img/fablab_iot_reunion.JPG)

L'objectif est de permettre aux agents et services de mieux connaître l'Internet des objets et son potentiel pour la collectivité. Par exemple, saviez-vous que l'Eurométropole disposait d'un réseau dédié pour l'internet des objets utilisable par tous (le fameux LoRa) ?

Grâce à cet objet connecté vous pourrez :

![Possibilité offertes par l'objet connecté](img/fablab_iot_usages2.JPG)

Et bien d'autres choses encore, laissez libre cours à votre imagination !

## Matériel utilisé

### Contrôleur

Utilisation de la carte contrôleur **Arduino MKR WAN 1300**.

### Capteurs

- **Luminosité** :

  - TSL2561 sur port TWI (adresse i2C : 0x76) [Librairie](https://github.com/Seeed-Studio/Grove_Digital_Light_Sensor/archive/master.zip)

- **Humidité, pression, gaz** :

  - BME680 sur port TWI (adresse i2C : 0x29) [Librairie](https://github.com/Seeed-Studio/Seeed_BME680)

- **Température et d'humidité** :

  - DHT22 sur port D2. [Librairie](https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor)

- **Qualité de l'air** :

  - Grove_Air_quality_Sensor sur port A0 : [Librairie](https://github.com/Seeed-Studio/Grove_Air_quality_Sensor)

  - Grove CO2 Sensor sur bus série : mesure de la température et du taux de CO2 ([Wiki](http://wiki.seeedstudio.com/Grove-CO2_Sensor/))

- **Concentration en poussières** :

  - HM3301 sur port TWI : mesure du taux de particules PM1.0, PM2.5 et PM10 ([Wiki](https://www.seeedstudio.com/Grove-Laser-PM2-5-Sensor-HM3301.html))

## Modalités de mise en oeuvre

### Connexion au réseau LoRa

Il est nécessaire de disposer d'un compte sur le serveur de gestion du réseau LoRa utilisé (loraszerver)
Les informations suivantes doivent être renseignées dans le fichier [arduino_secrets.h](Loratest1/arduino_secrets.h) :

- SECRET_APP_EUI : 0
- SECRET_APP_KEY : la clef générée par LORASERVER

### Pilotage des capteurs

Todo

### Configuration serveur

Todo
