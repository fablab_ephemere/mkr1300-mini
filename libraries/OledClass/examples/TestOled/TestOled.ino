#include <OledClass.h>

OledClass oled;

void setup() {
  oled.init();
}
char buf[20];

void loop() {
  // put your main code here, to run repeatedly:
  oled.rosace();
  delay(5000);
  oled.clear();
  delay(1000);
  oled.print(0,0, "1234567890123456");
  oled.print(5,0, "1234567890123456");
  oled.print(15,0, "1234567890123456");
  delay(5000);
}
