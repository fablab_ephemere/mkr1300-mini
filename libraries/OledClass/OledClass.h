#ifndef OledClass_h
#define OledClass_h
#include "Arduino.h"
#include <Wire.h>
#include <SeeedGrayOLED.h>
#include <avr/pgmspace.h>


class OledClass {
	public:
		OledClass();
		void init();
		void rosace();
		void print(int x, int y, char *str);
		void clear();
	private:
		int _maVar;
};
#endif