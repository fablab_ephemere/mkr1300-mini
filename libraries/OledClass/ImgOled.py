#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
from PIL import Image

if len(sys.argv) < 2:
    sys.exit("Veuillez préciser un nom de fichier (PNG, JPEG, etc) [et un niveau de luminosité entre 0 et 255, défaut 127]")

try:
    im = Image.open(sys.argv[1])
    pix = im.load()
except:
    sys.exit("Impossible d'ouvrir le fichier image")

if len(sys.argv) ==3:
    seuil = int(sys.argv[2])
else:
    seuil = 127

if seuil < 0 or seuil > 255:
    sys.exit("Le seuil doit être compris entre 0 et 255 !")
width, height = im.size

nom = os.path.splitext(sys.argv[1])[0]

if width > 128:
    print("largeur : " + str(width))
    sys.exit("Erreur : l'image doit avoir 128 pixels de large")
if height > 128:
    sys.exit("Erreur : l'image doit avoir 128 pixels de haut maximum")


print("// Image: "+ nom+"("+str(width)+"x"+str(height)+")")
print("#define L_" + nom + "     " + str(width))
print("#define H_" + nom + "     " + str(height))
x = 0
y = 0

print("\nstatic const unsigned char  " + nom + "[] PROGMEM ={")
while y < height:
    print("  ", end='')
    x = 0
    while x < width:
        val = 0
        val +=128 if pix[x,y] > 127 else 0
        val +=64 if pix[x+1,y] > 127 else 0
        val +=32 if pix[x+2,y] > 127 else 0
        val +=16 if pix[x+3,y] > 127 else 0
        val +=8 if pix[x+4,y] > 127 else 0
        val +=4 if pix[x+5,y] > 127 else 0
        val +=2 if pix[x+6,y] > 127 else 0
        val +=1 if pix[x+6,y] > 127 else 0
        if x == width - 8 and y == height - 1:
            print(str(hex(val)), end='')
        else:
            print(str(hex(val))+",", end='')
        x += 8
    print("")
    y += 1
print("};")