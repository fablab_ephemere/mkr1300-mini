# Librairie oled pour le Fablab

*Installer la librairie Oled pour l'afficheur [SH1107G][1]*

## Entête :
inclure :
    `#include <OledClass.h>`
déclarer une instance de la classe :
    `OledClass oled;`
	
	
## Corps :
Dans le setup :
	`oled.init();`
	
Les méthodes :
- oled.rosace() : affiche la rosace de la cathédrale de Strasbourg
- oled.clear() : efface l'afficheur
- oled.print(x, y, texte); : affiche un texte à la position x, y
	- x: la ligne (entre 0 et 15)
	- y: la colonne (entre 0 et 15)
	- texte : maximum 16 caractères en position 0

[1]: https://github.com/Seeed-Studio/OLED_Display_96X96/archive/master.zip
