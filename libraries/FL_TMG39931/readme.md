# Librairie fablab Grove Light & Gesture & Color & proximity sensor (TMG39931)

*Installer la librairie [TMG39931] [1]*
site Grove consultable [ici] [2] 

Connexion en I2C

## Entête :
inclure :
    `#include <TMG39931Class.h>`
déclarer une instance de la classe :
    `TMG39931Class tmg;`
	
	
## Corps :
Dans le setup :
	`tmg.init();`
Si le retour vaut 0 (faux) le module s'est mal initialisé
	
Les méthodes :
- void read_color() : effectue la lecture de la couleur, appeler ensuite les get correspondants
- uint16_t get_r() : la composante R
- uint16_t get_g() : la composante G
- uint16_t get_b() : la composante B
- uint32_t get_lux() : la luminosité
- uint32_t get_cct() : CCT

- uint8_t get_proximity(): la proximité au capteur



[1]: https://github.com/Seeed-Studio/Seeed_TMG3993.git
[2]: http://wiki.seeedstudio.com/Grove-Light-Gesture-Color-Proximity_Sensor-TMG39931/