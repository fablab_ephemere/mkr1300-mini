#include <TMG39931Class.h>


TMG3993 tmg3993;

TMG39931Class::TMG39931Class() {
	// constructeur
	
}

bool TMG39931Class::init() {
	bool ret;
	Wire.begin();
	ret = tmg3993.initialize();
	if (ret != false) {
		tmg3993.setADCIntegrationTime(0xdb); // the integration time: 103ms
		tmg3993.setupRecommendedConfigForProximity();
		tmg3993.enableEngines(ENABLE_PON | ENABLE_AEN | ENABLE_AIEN | ENABLE_PEN | ENABLE_PIEN);
	}
	return(ret);
}

void TMG39931Class::read_color() {
	tmg3993.getRGBCRaw(&_val.r, &_val.g, &_val.b, &_val.c);
	_val.lux = tmg3993.getLux(_val.r, _val.g, _val.b, _val.c);
    //the calculation of CCT is just from the `Application Note`,
    //from the result of our test, it might have error.
    _val.cct = tmg3993.getCCT(_val.r, _val.g, _val.b, _val.c);
}

uint16_t TMG39931Class::get_r() {
	return(_val.c);
}

uint16_t TMG39931Class::get_g() {
	return(_val.g);
}

uint16_t TMG39931Class::get_b() {
	return(_val.b);
}

uint32_t TMG39931Class::get_lux() {
	return(_val.lux);
}

uint32_t TMG39931Class::get_cct() {
	return(_val.cct);
}

uint8_t TMG39931Class::get_proximity() {
	 if (tmg3993.getSTATUS() & STATUS_PVALID) {
		uint8_t proximity_raw = tmg3993.getProximityRaw();  //read the Proximity data will clear the status bit
		return(proximity_raw);
	 }
	 return(-1);
}
