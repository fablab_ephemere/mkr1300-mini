#include <TMG39931Class.h>

TMG39931Class tmg;

void setup() {
  // put your setup code here, to run once:
  tmg.init();
  delay(5000);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  tmg.read_color();
  Serial.print("R : "); Serial.print(tmg.get_r());
  Serial.print("\tG : "); Serial.print(tmg.get_g());
  Serial.print("\tB : "); Serial.print(tmg.get_b());
  Serial.print("\tLux : "); Serial.print(tmg.get_lux());
  Serial.print("\tCCT : "); Serial.println(tmg.get_cct());
  Serial.print("Proximité : "); Serial.println(tmg.get_proximity());
  delay(2000);
}
