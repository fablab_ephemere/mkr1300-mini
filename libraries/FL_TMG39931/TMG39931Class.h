#ifndef TMG39931Class_h
#define TMG39931Class_h
#include "arduino.h"
#include <Wire.h>

#include "Seeed_TMG3993.h"



struct tmg_type {
	uint16_t r,g,b,c;
    int32_t lux, cct;
};


class TMG39931Class {
	public:
		TMG39931Class();
		bool init();
		void read_color(void);
		uint16_t get_r(void);
		uint16_t get_g(void);
		uint16_t get_b(void);
		uint32_t get_lux(void);
		uint32_t get_cct(void);
		uint8_t get_proximity(void);
	private:
		tmg_type _val;
};
#endif