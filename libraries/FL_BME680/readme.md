# Librairie BME680 pour le Fablab

*Installer la librairie [BME680] [1]*

## Entête :
inclure :
    `#include <BME680Class.h>`
déclarer une instance de la classe :
    `BME680Class bme;`
	
	
## Corps :
Dans le setup :
	`bme.init();`
Si le retour vaut 0 (faux) le module s'est mal initialisé
	
Les méthodes :
- bme.read_temp() : lit et retourne la température en degrés en type float
- bme.read_hum() : lit et retourne l'humidité en %
- bme.read_pres() : lit et retourne la pression atmosphérique en Pa
- bme.read_gaz() : lit et retourne la qualité de l'air :
	- 0-50    : bon 
	- 51-100  : moyen 
	- 101-150 : un peu mauvais 
	- 151-200 : mauvais 
	- 201-300 : pire 
	- 301-500 : très mauvais


[1]: https://github.com/Seeed-Studio/Seeed_BME680
