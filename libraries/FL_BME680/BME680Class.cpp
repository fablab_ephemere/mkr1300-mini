#include <BME680Class.h>

#define IIC_ADDR  uint8_t(0x76)
Seeed_BME680 _bme680(IIC_ADDR);

BME680Class::BME680Class() {
	// constructeur
	
}

bool BME680Class::init() {
	return(_bme680.init());
}

float BME680Class::read_temp(void) {
	return(_bme680.read_temperature());
}

float BME680Class::read_pres(void) {
	return(_bme680.read_pressure());
}
float BME680Class::read_hum(void) {
	return(_bme680.read_humidity());
}
float BME680Class::read_gaz(void) {
	return(_bme680.read_gas());
}

