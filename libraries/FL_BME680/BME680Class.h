#ifndef BME680Class_h
#define BME680Class_h
#include "arduino.h"
#include "seeed_bme680.h"

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10





class BME680Class {
	public:
		BME680Class();
		bool init();
		float read_temp(void);
		float read_pres(void);
		float read_hum(void);
		float read_gaz(void);
	private:
		
};
#endif