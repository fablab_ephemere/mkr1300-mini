#include <BME680Class.h>


BME680Class bme;

void setup() {
  Serial.begin(115200);
  Serial.println("Lecture BME680");
  while (!bme.init()) {
    Serial.println("Erreur d'initialisation");  
    delay(2000);
  }
}

void loop() {
  Serial.print("Température : "); Serial.println(bme.read_temp());
  Serial.print("Humidité    : "); Serial.println(bme.read_hum());
  Serial.print("Pression    : "); Serial.println(bme.read_pres());
  Serial.print("Gaz         : "); Serial.println(bme.read_gaz());
  Serial.println();
  delay(5000);
}
