struct Keys {
  String DEV_EUI;
  String APP_KEY;
  String APP_EUI;
};

//             DEV_EUI 8 octets    APPEUI 16 Octets                    APPEUI 8 octets
Keys key[] = {{"a8610a32332f670a", "10e2de3f3cf915908e1e578bd85f67d4", "0000000000000000"}, //MKRWAN Lora01
              {"a8610a323339880d", "e75507113113cfee6ab58cae84396443", "0000000000000000"}, //MKRWAN Lora02
              {"a8610a3233268c0d", "8eac235c99ace1e6e15f56dca01f12e5", "0000000000000000"}, //MKRWAN Lora03
              {"a8610a32332a9209", "bb0f0f182dbf02f5f79493e4e991bcbc", "0000000000000000"}, //MKRWAN Lora04 DPA
              {"a8610a3233448b0d", "55ec1a5d4f11b2559f675ba59cf49943", "0000000000000000"}, //MKRWAN Lora05
              {"a8610a32333d880d", "2def7592b7e175aaac1dc1e3d9968576", "0000000000000000"}, //MKRWAN Lora06 JME
              {"a8610a323333880d", "8e38e3f16fb34225d0fdfd93d14c52fb", "0000000000000000"}, //MKRWAN Lora07
              {"a8610a323335880d", "6d0853b411f5d1cd11125ced14a5c224", "0000000000000000"}, //MKRWAN Lora08
              {"a8610a323332880d", "1410c19a231c58fa3d88215b06c4ae3d", "0000000000000000"}, //MKRWAN Lora09 RLE
              {"a8610a323328890d", "3c7a4f39ce0008bfe1f55b338ce2b8d1", "0000000000000000"}, //MKRWAN Lora10
              {"a8610a3130467712", "ffbd4b6dcc82f3bd2d60a47f68e6e35c", "0000000000000000"}, //MKRWAN Lora11 PVG
              {"xxx", "xxxx", "0000000000000000"}, //MKRWAN Lora12
              {"xxx", "xxxx", "0000000000000000"}, //MKRWAN Lora13
              {"xxx", "xxxx", "0000000000000000"}, //MKRWAN Lora14
              {"a8610a3233446f0a", "c37ef6358b4363676736a4351285ff26", "0000000000000000"}  //MKRWAN Lora15 PVG
};
