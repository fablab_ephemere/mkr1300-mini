#include <MKRWAN.h>
#include <CayenneLPP.h>
#include "arduino_secrets.h"


/* 
Activation des traces de débogage 
Ne pas laisser si le capteur doit tourner en mode autonome, sans lien série
*/
//#define DEBUG  

boolean connected;
int err_count;
String appEui;
String appKey;

LoRaModem modem;
CayenneLPP lpp(51);


#ifdef DEBUG
  #define DEBUG_PRINTLN(x) Serial.println(x)
  #define DEBUG_PRINTLN2(x,y) Serial.println(x,y)
  #define DEBUG_PRINT(x) Serial.print(x)
  #define DEBUG_PRINT2(x,y) Serial.print(x,y)
  #define DEBUG_WAIT() while (!Serial); Serial.begin(115200)
  
#else
  #define DEBUG_PRINTLN(x)
  #define DEBUG_PRINTLN2(x,y)
  #define DEBUG_PRINT(x)
  #define DEBUG_PRINT2(x,y)
  #define DEBUG_WAIT()
#endif 



void setup() {
  
  DEBUG_WAIT();
  pinMode(LED_BUILTIN, OUTPUT);
  flashLed();

  Serial1.begin(9600);

  connected = false;
  err_count=0;
  if (!modem.begin(EU868)) {
    DEBUG_PRINTLN("Impossible de démarrer le module LORA");
    while (1) {}
  };
  delay(1000); //important
  
  DEBUG_PRINT("Le module LORA est en version : ");
  DEBUG_PRINTLN(modem.version());
  DEBUG_PRINT("Votre EUI est : ");
  DEBUG_PRINTLN(modem.deviceEUI());

  bool datarate = modem.dataRate(0); 
  if (!datarate) {
    DEBUG_PRINTLN("DataRate not set");
    while (1) {}
  }
  
  DEBUG_PRINT("Le datarate est: ");
  DEBUG_PRINTLN(modem.getDataRate());
  setLoraKeys(modem.deviceEUI());
  modem.dataRate(5);

  
}



void loop() {
  lpp.reset();
   DEBUG_PRINTLN("Attente du modem LoRa");
 
  if (!connected) {
      modem.minPollInterval(60);
      modem.setADR(true);
      int ret = modem.joinOTAA(appEui, appKey);
      DEBUG_PRINT("Appeui : ");
      DEBUG_PRINTLN(appEui);
      DEBUG_PRINT("AppKey : ");
      DEBUG_PRINTLN(appKey);
      
      if (ret) {
        connected = true;
        err_count=0;
        delay(100);
        DEBUG_PRINTLN("Connecté au réseau");
      } else {
        DEBUG_PRINTLN("Erreur connexion");
      }
      delay(5000);
  }

  if (connected) {
      int err;
      modem.beginPacket();
      dummyValue();
      modem.write(lpp.getBuffer(), lpp.getSize());
      err = modem.endPacket(false);
      // Vérification
      if (err > 0) {
        DEBUG_PRINTLN("Message bien envoyé");
        err_count=0;
        delay(1000);
        if (!modem.available()) {
          DEBUG_PRINTLN("Pas de message pour le moment");
        } else DEBUG_PRINTLN("Il y a un message");
      } else {
        err_count++;
        DEBUG_PRINT("Erreur n°");
        DEBUG_PRINT(err_count);
        DEBUG_PRINTLN(" lors de l'envoi du message :-(");
        if ( err_count >= 10 ) {
          connected = false;
          DEBUG_PRINTLN("Demande de reconnexion");
        }
      }
      delay(5000);
      flashLed();
      DEBUG_PRINTLN("Attente 120s");
      delay(120000); 
  }
  
}
