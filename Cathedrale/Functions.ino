void setLoraKeys(String thisDEV_EUI) {
  for (int i=0; i<sizeof(key)/sizeof(struct Keys); i++) {
    if (key[i].DEV_EUI == thisDEV_EUI) {
      appEui = key[i].APP_EUI;
      DEBUG_PRINT("APP_EUI: ");
      DEBUG_PRINTLN(appEui);
      appKey = key[i].APP_KEY;
      DEBUG_PRINT("APP_KEY: ");
      DEBUG_PRINTLN(appKey);
      return;
      }
    }
   DEBUG_PRINTLN("Pas de correspondance DEV_EUI trouvé ");
   while(1);
}

void flashLed(){
  digitalWrite(LED_BUILTIN, HIGH);   
  delay(1000);                       
  digitalWrite(LED_BUILTIN, LOW);    
  delay(1000);                       
}

void dummyValue() {
    lpp.addTemperature(2, (float)20); 
}
