#include <MKRWAN.h>
#include <CayenneLPP.h>
#include "arduino_secrets.h"

#include <OledClass.h>
#include <BME680Class.h>

OledClass oled;
BME680Class bme;

/* 
Activation des traces de débogage 
Ne pas laisser si le capteur doit tourner en mode autonome, sans lien série
*/
//#define DEBUG  

boolean connected;
int err_count;
String appEui;
String appKey;

LoRaModem modem;
CayenneLPP lpp(51);


#ifdef DEBUG
  #define DEBUG_PRINTLN(x) Serial.println(x)
  #define DEBUG_PRINTLN2(x,y) Serial.println(x,y)
  #define DEBUG_PRINT(x) Serial.print(x)
  #define DEBUG_PRINT2(x,y) Serial.print(x,y)
  #define DEBUG_WAIT() while (!Serial); Serial.begin(115200)
  
#else
  #define DEBUG_PRINTLN(x)
  #define DEBUG_PRINTLN2(x,y)
  #define DEBUG_PRINT(x)
  #define DEBUG_PRINT2(x,y)
  #define DEBUG_WAIT()
#endif 

char buf[20];
float tmp, hum, pres, voc;
String stat;
int nb_msg = 0;
int offst;

void setup() {
  DEBUG_WAIT();
  pinMode(LED_BUILTIN, OUTPUT);
  flashLed();
  
  oled.init();
  while (!bme.init()) {
    oled.clear();
    oled.print(7,0, "ERR INIT");  
    delay(2000);
  }
  Serial1.begin(9600);

  connected = false;
  err_count=0;
  if (!modem.begin(EU868)) {
    DEBUG_PRINTLN("Impossible de démarrer le module LORA");
    while (1) {}
  };
  delay(1000); //important
  
  DEBUG_PRINT("Le module LORA est en version : ");
  DEBUG_PRINTLN(modem.version());
  DEBUG_PRINT("Votre EUI est : ");
  DEBUG_PRINTLN(modem.deviceEUI());

  bool datarate = modem.dataRate(0); 
  if (!datarate) {
    DEBUG_PRINTLN("DataRate not set");
    while (1) {}
  }
  
  DEBUG_PRINT("Le datarate est: ");
  DEBUG_PRINTLN(modem.getDataRate());
  setLoraKeys(modem.deviceEUI());

  
}



void loop() {
  lpp.reset();
   DEBUG_PRINTLN("Attente du modem LoRa");
  oled.clear();
  oled.print(8, 0, "  Attente LORA  ");
  if (!connected) {
      modem.minPollInterval(60);
      modem.setADR(true);
      int ret = modem.joinOTAA(appEui, appKey);
      DEBUG_PRINT("Appeui : ");
      DEBUG_PRINTLN(appEui);
      DEBUG_PRINT("AppKey : ");
      DEBUG_PRINTLN(appKey);
      
      if (ret) {
        connected = true;
        err_count=0;
        delay(100);
        DEBUG_PRINTLN("Connecté au réseau");
        oled.print(8, 0, "... Connexion ..");
      } else {
        DEBUG_PRINTLN("Erreur connexion");
        oled.print(8, 0, "Err de connexion");
      }
      delay(5000);
  }

  if (connected) {
      int err;
      oled.clear();
      oled.rosace();
      tmp = bme.read_temp();
      hum = bme.read_hum();
      pres = bme.read_pres()/1000.0;
      voc = bme.read_gaz()/1000.0;
      DEBUG_PRINT("Température : "); DEBUG_PRINTLN(tmp);
      DEBUG_PRINT("Humidité : "); DEBUG_PRINTLN(hum);
      DEBUG_PRINT("Pression (hPa) : "); DEBUG_PRINTLN(pres);
      DEBUG_PRINT("VOC (kohms) : "); DEBUG_PRINTLN(voc);

      modem.beginPacket();
      lpp.addTemperature(5, tmp);
      lpp.addBarometricPressure(6, pres);
      lpp.addRelativeHumidity(7, hum);
      lpp.addAnalogInput(8, voc);
      modem.write(lpp.getBuffer(), lpp.getSize());
      err = modem.endPacket(false);
      // Vérification
      if (err > 0) {
        DEBUG_PRINTLN("Message bien envoyé");
        stat = "envoye";
        nb_msg++;
        err_count=0;
        delay(1000);
        if (!modem.available()) {
          DEBUG_PRINTLN("Pas de message pour le moment");
        } else DEBUG_PRINTLN("Il y a un message");
      } else {
        err_count++;
        DEBUG_PRINT("Erreur n°");
        DEBUG_PRINT(err_count);
        DEBUG_PRINTLN(" lors de l'envoi du message :-(");
        if ( err_count >= 10 ) {
          connected = false;
          DEBUG_PRINTLN("Demande de reconnexion");
        }
        stat = "Err " + err_count;
      }
      delay(5000);
      flashLed();
      DEBUG_PRINTLN("Attente 10mn");
      for (int i=0; i<30; i++) {
          oled.clear();
          sprintf(buf, "Temp %d.%d", int(tmp), int(tmp * 10 - (int(tmp) * 10))); 
          oled.print(4, 4, buf);
          sprintf(buf, "Hum %d %%", int(hum));
          oled.print(6, 5, buf);
          sprintf(buf, "Pa %d hPa", int(pres*10));
          oled.print(8, 3, buf);
          sprintf(buf, "VOC %d kohms", int(voc));
          oled.print(10, 2, buf);
          sprintf(buf, "%d msg", nb_msg);
          offst = 8 - strlen(buf)/2;
          oled.print(12, offst, buf);
          stat.toCharArray(buf, 16);
          oled.print(13, 5, buf);
          delay(10000);
          oled.clear();
          oled.rosace();
          delay(10000);
      }
  }
  
}
