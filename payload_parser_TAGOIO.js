// Add ignorable variables in this array.
const ignore_vars = ['applicationID','applicationName','deviceName','devEUI','rxInfo','txInfo','adr','fCnt','fPort','object'];
	const LPP_TYPE_DIGITAL_INPUT = 0x00;
	const LPP_TYPE_DIGITAL_OUTPUT = 0x01;
	const LPP_TYPE_ANALOG_INPUT = 0x02;
	const LPP_TYPE_ANALOG_OUTPUT = 0x03;
	const LPP_TYPE_ILLUMINANCE_SENSOR = 0x65;
	const LPP_TYPE_PRESENCE_SENSOR = 0x66;
	const LPP_TYPE_TEMPERATURE_SENSOR = 0x67;
	const LPP_TYPE_HUMIDITY_SENSOR = 0x68;
	const LPP_TYPE_ACCELEROMETER = 0x71;
	const LPP_TYPE_MAGNETOMETER = 0x72;
	const LPP_TYPE_BAROMETER = 0x73;
	const LPP_TYPE_GYROMETER = 0x86;
	const LPP_TYPE_GPS_LOCATION = 0x88;
/**
 * Convert an object to TagoIO object format.
 * Can be used in two ways:
 * toTagoFormat({ myvariable: myvalue , anothervariable: anothervalue... })
 * toTagoFormat({ myvariable: { value: myvalue, unit: 'C', metadata: { color: 'green' }} , anothervariable: anothervalue... })
 *
 * @param {Object} object_item Object containing key and value.
 * @param {String} serie Serie for the variables
 * @param {String} prefix Add a prefix to the variables name
 */

function toTagoFormat(object_item, serie, prefix = '') {
  const result = [];
  for (const key in object_item) {
    if (ignore_vars.includes(key)) continue; // ignore chosen vars

    if (typeof object_item[key] == 'object') {
      result.push({
        variable: object_item[key].variable || `${prefix}${key}`,
        value: object_item[key].value,
        serie: object_item[key].serie || serie,
        metadata: object_item[key].metadata,
        unit: object_item[key].unit,
        location: object_item[key].location,
      });
    } else {
      result.push({
        variable: `${prefix}${key}`,
        value: object_item[key],
        serie,
      });
    }
  }

  return result;
}


function parsePayload(payload_raw) {
  const buffer = Buffer.from(payload_raw, 'base64');
  let data = {};
  let i=0;
	let channel = 0;
	let car = 0;
	let property = "";
	let value = 0;
	while (i< buffer.length) {
		channel = buffer.slice(i,i+1).readUInt8();
		car = buffer.slice(i+1,i+2).readUInt8();
		i +=2;
		switch (car) {
		  case LPP_TYPE_ANALOG_INPUT:
				property = 'analog_input';
				value = buffer.slice(i, i+2).readInt16BE() / 100.0;
				i += 2;
				if (channel == 4)
					data["AirQUAL"]={value: value, unit: "air"};
				else
					data["BME680GAZ"]={value: value, unit: "Kohms"};
				break;
		  case LPP_TYPE_ILLUMINANCE_SENSOR:
				value = buffer.slice(i, i+2).readInt16BE() / 1.0;
				i += 2;
				data["TSL2561LUM"]={value: value, unit: "LUX"};
				break;
		  case LPP_TYPE_TEMPERATURE_SENSOR:
				value = buffer.slice(i, i+2).readInt16BE() / 10.0;
				i += 2;
				if (channel == 2)
					data["DHT22TEMP"]={value: value, unit: "°C"};
				else
					data["BME680TEMP"]={value: value, unit: "°C"};
				break;
		  case LPP_TYPE_HUMIDITY_SENSOR:
				value = buffer.slice(i, i+1).readUInt8() / 2.0;
				i++;
				if (channel == 1)
					data["DHT22HUM"]={value: value, unit: "%"};
				else
					data["BME680HUM"]={value: value, unit: "%"};
				break;
		  case LPP_TYPE_BAROMETER:
				property = 'barometer';
				value = buffer.slice(i, i+2).readInt16BE();
				i +=2;
				data["BME680PRES"]={value: value, unit: "hPa"};
				break;
			case LPP_TYPE_GPS_LOCATION:
       	lat = (buffer.readInt32BE(i) >> 8) / 10000.0;
				i += 3;
				lng = (buffer.readInt32BE(i) >> 8) / 10000.0;
				i += 3;
				data["Coordonnees"] = {location: {"lat": lat,"lng": lng}};
				alt = (buffer.readInt32BE(i) >> 8) / 100.0;
     		data["altitude"]={value: alt, unit: "m"};
        i += 3;
        break;
	default:
			data["defaut"]={value: car, unit: "num"};
			i++;
		}
	}
  return data;
}

//converti le json raw en json "Tago"
if (!payload[0].variable) {
  const serie = payload[0].serie || new Date().getTime();

  payload = toTagoFormat(payload[0], serie);
}


//recupere la variable data pour parser le format cayenne LPP
const my_payload = payload.find(x => x.variable === 'data');
if (my_payload) {
  const { serie, value } = my_payload;
  try {
    const parsed_payload = parsePayload(value);
    payload = payload.concat(toTagoFormat(parsed_payload, serie));
  } catch (e) {
    payload = {  variable: 'parse_error', value: e.message || e };
  }
}
